package org.bitbucket.kles4enko.ld33.desktop;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.kles4enko.ld33.core.GameWorld;
import org.bitbucket.kles4enko.ld33.core.SceneGameWorld;
import org.bitbucket.kles4enko.ld33.core.util.Constants;
import playn.java.JavaPlatform;
import playn.java.LWJGLPlatform;

/**
 * @author Andrei Kleshchanka
 */
public class TrueMonsterDesktop {

    private static final Logger LOG = LogManager.getLogger(TrueMonsterDesktop.class);

    public static void main(String[] args) {
        LOG.info("[Starting Game]");
        LWJGLPlatform.Config config = new JavaPlatform.Config();
        config.appName = Constants.APP_NAME;
        config.convertImagesOnLoad = true;
        config.fullscreen = Constants.IS_FULL_SCREEN;
        config.width = Constants.WINDOW_WIDTH;
        config.height = Constants.WINDOW_HEIGHT;
        LWJGLPlatform platform = new LWJGLPlatform(config);
        new SceneGameWorld(platform, Constants.UPDATE_RATE);
        LOG.info("start!");
        platform.start();
    }
}
