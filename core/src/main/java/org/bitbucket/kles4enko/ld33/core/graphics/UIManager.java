package org.bitbucket.kles4enko.ld33.core.graphics;

import playn.core.Canvas;
import playn.core.Color;
import playn.core.Platform;
import playn.core.Surface;
import tripleplay.ui.Background;
import tripleplay.ui.Label;
import tripleplay.ui.Style;
import tripleplay.util.Colors;

/**
 * @author Andrei Kleshchanka
 */
public class UIManager {

    private final Platform plat;
    private final Canvas   canvas;

    public UIManager(Platform plat) {
        this.plat = plat;
        this.canvas = plat.graphics().createCanvas(plat.graphics().viewSize.width(), plat.graphics().viewSize.height());
    }

    public void render(Surface surface, String text, float x, float y) {
        canvas.clear();
        canvas.setFillColor(Colors.WHITE);
        canvas.drawText(text, x, y);
        surface.draw(canvas.toTexture(), 0, 0);
    }

    public void renderWithBlackBg(Surface surface, String text, float x, float y, float width, float height) {
        canvas.clear();
        canvas.setFillColor(Colors.BLACK);
        canvas.fillRect(x - 12, y - 16, width, height);
        canvas.setFillColor(Color.rgb(255, 255, 255));
        canvas.drawText(text, x, y);
        surface.draw(canvas.toTexture(), 0, 0);
    }

    protected Label label(String text) {
        return new Label(text).addStyles(Style.HALIGN.center, Style.TEXT_WRAP.on);
    }

    protected Label label(String text, Background bg) {
        return label(text).addStyles(Style.BACKGROUND.is(bg));
    }
}
