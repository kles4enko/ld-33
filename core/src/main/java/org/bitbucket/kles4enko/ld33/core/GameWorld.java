package org.bitbucket.kles4enko.ld33.core;

import com.google.common.base.MoreObjects;
import com.google.common.base.Stopwatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.kles4enko.ld33.core.control.MonsterController;
import org.bitbucket.kles4enko.ld33.core.entities.EntitiesSortStrategy;
import org.bitbucket.kles4enko.ld33.core.entities.EntitiesSortStrategyImpl;
import org.bitbucket.kles4enko.ld33.core.entities.IsoEntity;
import org.bitbucket.kles4enko.ld33.core.entities.Player;
import org.bitbucket.kles4enko.ld33.core.graphics.Camera;
import org.bitbucket.kles4enko.ld33.core.graphics.Renderer;
import org.bitbucket.kles4enko.ld33.core.graphics.TileManager;
import org.bitbucket.kles4enko.ld33.core.level.Level;
import org.bitbucket.kles4enko.ld33.core.level.LevelLoader;
import org.bitbucket.kles4enko.ld33.core.level.LevelProvider;
import org.bitbucket.kles4enko.ld33.core.logic.CollisionSystem;
import org.bitbucket.kles4enko.ld33.core.logic.Updater;
import org.bitbucket.kles4enko.ld33.core.sound.SoundManager;
import playn.core.*;
import react.Connection;
import react.Slot;
import tripleplay.game.ScreenStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static org.bitbucket.kles4enko.ld33.core.util.PlayNUtil.createDefaultBatch;

/**
 * @author Andrei Kleshchanka
 */
public class GameWorld extends Game {

    private static final Logger LOG = LogManager.getLogger(GameWorld.class);

    private final Platform             plat;
    private final ScreenStack          screens;
    private final ScreenStack.UIScreen gameScreen;
    private final SoundManager         soundManager;
    // graphics
    private final QuadBatch            quadBatch;
    private final Surface              surface;
    private final Renderer             renderer;
    private final Camera               camera;
    //  logic
    private final Updater              updater;
    private final CollisionSystem      collisionSystem;
    private final EntitiesSortStrategy sortStrategy;
    private final MonsterController    controller;
    private final Stopwatch            stopwatch;
    private final TileManager tileManager;
    private Level           currentLevel;
    private List<IsoEntity> map;
    private Player          player;
    private Connection      updateConnection;
    private Connection      paintConnection;

    public GameWorld(Platform plat, int updateRate, ScreenStack screens, ScreenStack.UIScreen gameScreen, SoundManager soundManager) {
        super(plat, updateRate);
        this.plat = plat;
        this.screens = screens;
        this.gameScreen = gameScreen;
        this.soundManager = soundManager;
        this.stopwatch = Stopwatch.createUnstarted();
        // level
        this.map = new ArrayList<>();
        // graphics
        this.camera = new Camera(plat.graphics().viewSize);
        this.quadBatch = createDefaultBatch(plat.graphics().gl);
        this.surface = new Surface(plat.graphics(), plat.graphics().defaultRenderTarget, quadBatch);
        // level
        this.tileManager = new TileManager(plat);
        LevelLoader loader = new LevelLoader(plat, new LevelProvider().levelNameById(1), tileManager);
        currentLevel = loader.loadLevel();
        // logic
        this.player = currentLevel.getPlayer();
        this.renderer = new Renderer(surface, plat, camera, stopwatch, player);
        this.controller = new MonsterController(plat, player, this, stopwatch, currentLevel, tileManager);
        this.collisionSystem = new CollisionSystem();
        this.sortStrategy = new EntitiesSortStrategyImpl();
        this.updater = new Updater(controller, sortStrategy, collisionSystem, camera, player, stopwatch);
        LOG.info("Initialized : {}", this);
    }

    public void unConnect() {
        if (Objects.nonNull(updateConnection)) updateConnection.close();
        if (Objects.nonNull(paintConnection)) paintConnection.close();
        soundManager.getGameSound().stop();
        screens.remove(gameScreen, screens.slide().left());
    }

    public void initLoopListeners() {
        updateConnection = update.connect(new Slot<Clock>() {
            @Override
            public void onEmit(Clock clock) {
                LOG.trace("Update : {}", clock);
                currentLevel = updater.update(clock, currentLevel);
            }
        });
        paintConnection = paint.connect(new Slot<Clock>() {
            @Override
            public void onEmit(Clock renderClock) {
                LOG.trace("Paint : {}", renderClock);
                renderer.render(renderClock, currentLevel.getMap());
            }
        });
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .omitNullValues()
                .add("plat", plat)
                .add("quadBatch", quadBatch)
                .add("surface", surface)
                .add("renderer", renderer)
                .add("updater", updater)
                .add("map", map)
                .add("player", player)
                .toString();
    }
}
