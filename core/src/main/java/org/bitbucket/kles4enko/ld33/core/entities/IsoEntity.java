package org.bitbucket.kles4enko.ld33.core.entities;

import com.google.common.base.MoreObjects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import playn.core.Image;
import playn.core.Tile;
import pythagoras.f.IRectangle;
import pythagoras.f.Rectangle;
import pythagoras.f.Vector3;
import react.RFuture;
import react.Slot;
import react.Try;

import java.util.Objects;

/**
 * @author Andrei Kleshchanka
 */
public class IsoEntity {

    private static final Logger LOG = LogManager.getLogger(IsoEntity.class);
    protected final char    tileType;
    protected       Tile    tile;
    protected       Vector3 isoPosition;
    protected       float   isoDepth;

    public IsoEntity(Vector3 isoPosition, float isoDepth, RFuture<Image> imageState, char tileType) {
        this.isoPosition = isoPosition;
        this.isoDepth = isoDepth;
        this.tileType = tileType;
        imageState.onComplete(new Slot<Try<Image>>() {
            @Override
            public void onEmit(Try<Image> event) {
                tile = event.get().tile();
            }
        });
    }

    public IsoEntity(Vector3 isoPosition, float isoDepth, Tile tile, char tileType) {
        this.tileType = tileType;
        this.tile = tile;
        this.isoPosition = isoPosition;
        this.isoDepth = isoDepth;
    }

    public Tile getTile() {
        return tile;
    }

    public void setTile(Tile tile) {
        this.tile = tile;
    }

    public Vector3 getIsoPosition() {
        return isoPosition;
    }

    public void setIsoPosition(Vector3 isoPosition) {
        this.isoPosition = isoPosition;
    }

    public float getIsoDepth() {
        return isoDepth;
    }

    public void setIsoDepth(float isoDepth) {
        this.isoDepth = isoDepth;
    }

    public boolean canMove() {
        return false;
    }

    public boolean isPlayer() {
        return false;
    }

    public boolean isEnemy() {return false;}

    public boolean isEmptyFloor() {
        return ' ' == tileType;
    }

    public char getTileType() {
        return tileType;
    }

    public IRectangle getRectangle() {
        return new Rectangle(isoPosition.x, isoPosition.y, 1F, 1F);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IsoEntity entity = (IsoEntity) o;
        return Objects.equals(isoDepth, entity.isoDepth) &&
                Objects.equals(tile, entity.tile) &&
                Objects.equals(isoPosition, entity.isoPosition);
    }

    @Override
    public int hashCode() {
        return Objects.hash(tile, isoPosition, isoDepth);
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("tileType", tileType)
                .add("tile", tile)
                .add("isoPosition", isoPosition)
                .add("isoDepth", isoDepth)
                .toString();
    }
}

