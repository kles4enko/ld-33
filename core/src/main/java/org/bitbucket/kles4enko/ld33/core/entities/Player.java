package org.bitbucket.kles4enko.ld33.core.entities;

import com.google.common.base.MoreObjects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import playn.core.Image;
import playn.core.Tile;
import pythagoras.f.Vector3;
import react.RFuture;

/**
 * @author Andrei Kleshchanka
 */
public class Player extends MovableIsoEntity {

    private static final Logger LOG   = LogManager.getLogger(Player.class);
    private              int    kills = 0;

    public Player(Vector3 isoPosition, float isoDepth, RFuture<Image> imageState, char tileType) {
        super(isoPosition, isoDepth, imageState, tileType);
    }

    public Player(Vector3 isoPosition, float isoDepth, Tile tile, Vector3 position, char tileType) {
        super(isoPosition, isoDepth, tile, tileType);
    }

    public void enemyKilled() {
        kills++;
    }

    public int getScore() {
        return kills;
    }

    @Override
    public boolean isPlayer() {
        return true;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .toString();
    }
}
