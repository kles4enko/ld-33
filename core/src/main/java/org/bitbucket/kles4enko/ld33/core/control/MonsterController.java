package org.bitbucket.kles4enko.ld33.core.control;

import com.google.common.base.MoreObjects;
import com.google.common.base.Stopwatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.kles4enko.ld33.core.GameWorld;
import org.bitbucket.kles4enko.ld33.core.entities.Enemy;
import org.bitbucket.kles4enko.ld33.core.entities.IsoEntity;
import org.bitbucket.kles4enko.ld33.core.entities.Player;
import org.bitbucket.kles4enko.ld33.core.graphics.TileManager;
import org.bitbucket.kles4enko.ld33.core.level.Level;
import org.bitbucket.kles4enko.ld33.core.util.Constants;
import playn.core.Clock;
import playn.core.Image;
import playn.core.Keyboard;
import playn.core.Platform;
import pythagoras.f.Vector3;

import java.util.List;
import java.util.stream.Collectors;

import static org.bitbucket.kles4enko.ld33.core.util.IsoUtil.*;

/**
 * @author Andrei Kleshchanka
 */
public class MonsterController {

    private static final Logger LOG = LogManager.getLogger(MonsterController.class);

    private final Platform    plat;
    private final Player      player;
    private final GameWorld   gameWorld;
    private final Stopwatch   stopwatch;
    private final Level       currentLevel;
    private final TileManager tileManager;

    private boolean isUpPressed;
    private boolean isDownPressed;
    private boolean isLeftPressed;
    private boolean isRightPressed;
    private boolean isActionPressed;

    public MonsterController(Platform plat, Player player, GameWorld gameWorld, Stopwatch stopwatch, Level currentLevel, TileManager tileManager) {
        this.plat = plat;
        this.player = player;
        this.gameWorld = gameWorld;
        this.stopwatch = stopwatch;
        this.currentLevel = currentLevel;
        this.tileManager = tileManager;
        configureKeyListeners();
        LOG.info("Initialized : {}", this);
    }

    private void configureKeyListeners() {
        plat.input().keyboardEvents.connect(new Keyboard.KeySlot() {
            @Override
            public void onEmit(Keyboard.KeyEvent keyEvent) {
                switch (keyEvent.key) {
                    case UP: { // up
                        isUpPressed = keyEvent.down;
                        startStopWatch();
                        break;
                    }
                    case DOWN: { // down
                        isDownPressed = keyEvent.down;
                        startStopWatch();
                        break;
                    }
                    case LEFT: { // left
                        isLeftPressed = keyEvent.down;
                        startStopWatch();
                        break;
                    }
                    case RIGHT: { // right
                        isRightPressed = keyEvent.down;
                        startStopWatch();
                        break;
                    }
                    case Z: { // action
                        isActionPressed = keyEvent.down;
                        startStopWatch();
                        action();
                        break;
                    }
                    case ESCAPE: {
                        LOG.debug("Esc");
                        gameWorld.unConnect();
                        break;
                    }
                }
            }
        });
    }

    private void action() {
        final Vector3 normPos = normalizedTilePosition(player.getIsoPosition());
        final Vector3 neighN = getNeighN(normPos);
        final Vector3 neighE = getNeighE(normPos);
        final Vector3 neighS = getNeighS(normPos);
        final Vector3 neighW = getNeighW(normPos);

        final List<IsoEntity> enemies = currentLevel.getMap().stream()
                .filter(IsoEntity::canMove)
                .filter(IsoEntity::isEnemy)
                .filter(toKill -> {
                    Vector3 isoPosition = normalizedTilePosition(toKill.getIsoPosition());
                    return isoPosition.equals(neighN) || isoPosition.equals(neighN) || isoPosition.equals(neighE) || isoPosition.equals(neighS) || isoPosition.equals(neighW) || isoPosition.equals(normPos);
                })
                .collect(Collectors.toList());
        currentLevel.getMap().removeAll(enemies);

        enemies.stream().forEach(enemy -> {
            Image doctorDied = tileManager.getTile('d');
            Enemy enemyNew = new Enemy(enemy.getIsoPosition(), enemy.getIsoDepth(), doctorDied.state, 'd');
            enemyNew.setCanNotMove();
            currentLevel.getMap().add(enemyNew);
            player.enemyKilled();
        });

    }

    private void startStopWatch() {
        if (!stopwatch.isRunning()) {
            stopwatch.start();
        }
    }

    public void update(Clock clock) {
        float step = Constants.MONSTER_SPEED;
        if (isUpPressed) player.getIsoPosition().addLocal(0, -step, 0);
        if (isDownPressed) player.getIsoPosition().addLocal(0, step, 0);
        if (isLeftPressed) player.getIsoPosition().addLocal(-step, 0, 0);
        if (isRightPressed) player.getIsoPosition().addLocal(step, 0, 0);
        if (isActionPressed) {/*TODO: to be implement*/
            LOG.debug("Action!");
        }
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("isActionPressed", isActionPressed)
                .add("plat", plat)
                .add("player", player)
                .add("isUpPressed", isUpPressed)
                .add("isDownPressed", isDownPressed)
                .add("isLeftPressed", isLeftPressed)
                .add("isRightPressed", isRightPressed)
                .toString();
    }
}
