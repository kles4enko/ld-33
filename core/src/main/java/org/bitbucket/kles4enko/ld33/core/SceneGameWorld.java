package org.bitbucket.kles4enko.ld33.core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.kles4enko.ld33.core.sound.SoundManager;
import playn.core.*;
import playn.scene.Pointer;
import playn.scene.SceneGame;
import react.UnitSlot;
import tripleplay.game.ScreenStack;
import tripleplay.ui.*;
import tripleplay.ui.layout.AxisLayout;
import tripleplay.util.Colors;


/**
 * @author Andrei Kleshchanka
 */
public class SceneGameWorld extends SceneGame {

    private static final Logger LOG = LogManager.getLogger(SceneGameWorld.class);
    private final ScreenStack          screens;
    private final ScreenStack.UIScreen gameScreen;
    private final ScreenStack.UIScreen introScreen;
    private       GameWorld            gameWorld;
    private final SoundManager soundManager;

    public SceneGameWorld(Platform plat, int updateRate) {
        super(plat, updateRate);
        setClearColor(32 / 255F, 32 / 255F, 32 / 255F, 1F);
        new Pointer(plat, rootLayer, true);
        this.screens = new ScreenStack(this, rootLayer);
        this.soundManager = new SoundManager(plat);

        this.introScreen = new ScreenStack.UIScreen(plat) {
            @Override
            public void wasAdded() {
                LOG.debug("IntroScreen was added");
                int bgColor = Color.rgb(32, 32, 32);
                float width = plat.graphics().viewSize.width();
                float height = plat.graphics().viewSize.height();

                Root root = iface.createRoot(AxisLayout.vertical(), SimpleStyles.newSheet(game().plat.graphics()), layer);
                root.addStyles(Style.BACKGROUND.is(Background.solid(bgColor)), Style.VALIGN.top);
                root.setSize(size());
                Group main = new Group(AxisLayout.vertical());
                root.add(main);

                main.add(new Label("\t"));
                main.add(new Label("\t"));
                main.add(new Label("\t"));
                main.add(new Label("\t"));
                main.add(new Label("\t"));
                main.add(new Label("\t"));

                main.add(new Label("True Monster").addStyles(Style.TEXT_EFFECT.pixelOutline, Style.FONT.is(new Font("Helvetica", 24))));
                main.add(new Label("\t"));
                main.add(new Label("You are a young white man.").addStyles(Style.COLOR.is(Colors.WHITE)));
                main.add(new Label("Once, there comes day when you become a father.").addStyles(Style.COLOR.is(Colors.WHITE)));
                main.add(new Label("You rush in maternity hospital for the child.").addStyles(Style.COLOR.is(Colors.WHITE)));
                main.add(new Label("And you are handed IT - is the RED BLACK kid").addStyles(Style.COLOR.is(Colors.WHITE)));
                main.add(new Label("WTF?!?!?!?!").addStyles(Style.COLOR.is(Colors.WHITE)));
                main.add(new Label("And you fall into rage.... Now you are a monster...").addStyles(Style.COLOR.is(Colors.WHITE)));

                main.add(new Label("\t"));

                main.add(new Button("Start!").onClick(new UnitSlot() {
                    public void onEmit() {
                        LOG.debug("Run on click");
                        soundManager.getIntroSound().stop();
                        screens.push(gameScreen, screens.slide().left());
                        soundManager.getGameSound().play();
                    }
                }));
            }

            @Override
            public void wasRemoved() {
                LOG.debug("IntroScreen was removed");
            }


            @Override
            public Game game() {
                return gameWorld;
            }
        };
        gameScreen = new ScreenStack.UIScreen(plat) {

            @Override
            public void wasAdded() {
                LOG.debug("Game screen was added");
                gameWorld.initLoopListeners();
            }

            @Override
            public void wasRemoved() {
                LOG.debug("Game screen was removed");
                soundManager.getIntroSound().play();
                gameWorld = new GameWorld(plat, 33, screens, gameScreen, soundManager);
            }

            @Override
            public Game game() {
                return gameWorld;
            }

        };
        gameWorld = new GameWorld(plat, 33, screens, gameScreen, soundManager);
        screens.push(introScreen, screens.slide());
    }
}
