package org.bitbucket.kles4enko.ld33.core.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import playn.core.*;

/**
 * @author Andrei Kleshchanka
 */
public class PlayNUtil {

    public static final Texture.Config PIXELART_TEXTURE_CONFIG = new Texture.Config(false, false, false, GL20.GL_NEAREST, GL20.GL_NEAREST, false);

    private static final Logger LOG = LogManager.getLogger(PlayNUtil.class);

    public static QuadBatch createDefaultBatch(GL20 gl) {
        try {
            if (UniformQuadBatch.isLikelyToPerform(gl)) return new UniformQuadBatch(gl);
        } catch (Exception e) {
            LOG.error("Couldn't create Uniform Quad Batch", e);
        }
        return new TriangleBatch(gl);
    }
}
