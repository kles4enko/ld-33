package org.bitbucket.kles4enko.ld33.core.entities;

import com.google.common.base.MoreObjects;
import playn.core.Image;
import playn.core.Tile;
import pythagoras.f.Vector3;
import react.RFuture;

/**
 * @author Andrei Kleshchanka
 */
public class Enemy extends MovableIsoEntity {

    private boolean canMove = true;

    public Enemy(Vector3 isoPosition, float isoDepth, RFuture<Image> imageState, char tileType) {
        super(isoPosition, isoDepth, imageState, tileType);
    }

    public Enemy(Vector3 isoPosition, float isoDepth, Tile tile, char tileType) {
        super(isoPosition, isoDepth, tile, tileType);
    }

    @Override
    public boolean isEnemy() {
        return true;
    }

    public void setCanNotMove() {
        this.canMove = false;
    }

    @Override
    public boolean canMove() {
        return canMove;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this).toString();
    }
}
