package org.bitbucket.kles4enko.ld33.core.entities;

import com.google.common.base.MoreObjects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import playn.core.Image;
import playn.core.Tile;
import pythagoras.f.Vector3;
import react.RFuture;

/**
 * @author Andrei Kleshchanka
 */
public class MovableIsoEntity extends IsoEntity {

    private static final Logger LOG = LogManager.getLogger(MovableIsoEntity.class);

    public MovableIsoEntity(Vector3 isoPosition, float isoDepth, RFuture<Image> imageState, char tileType) {
        super(isoPosition, isoDepth, imageState, tileType);
    }

    public MovableIsoEntity(Vector3 isoPosition, float isoDepth, Tile tile, char tileType) {
        super(isoPosition, isoDepth, tile, tileType);
    }

    @Override
    public boolean canMove() {
        return true;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .toString();
    }
}
