package org.bitbucket.kles4enko.ld33.core.level;

/**
 * @author Andrei Kleshchanka
 */
public class LevelProvider {

    public String levelNameById(int levelId) {
        return String.format("levels/level_%s.txt", levelId);
    }
}
