package org.bitbucket.kles4enko.ld33.core.util;

/**
 * @author Andrei Kleshchanka
 */
public final class Constants {

    // app
    public static final String  APP_NAME             = "True Monster";
    public static final int     WINDOW_WIDTH         = 1024;
    public static final int     WINDOW_HEIGHT        = 768;
    public static final int     UPDATE_RATE          = 45;
    public static final boolean IS_FULL_SCREEN       = false;
    // tile
    public static final float   TILE_WIDTH           = 64;
    public static final float   TILE_HEIGHT          = 32;
    public static final float   TILE_WIDTH_HALF      = TILE_WIDTH / 2;
    public static final float   TILE_HEIGHT_HALF     = TILE_HEIGHT / 2;
    // messages
    public static final String  MESSAGES_BUNDLE_PATH = "assets/messages/default";
    // monster
    public static final float   MONSTER_SPEED        = 0.33F;
}
