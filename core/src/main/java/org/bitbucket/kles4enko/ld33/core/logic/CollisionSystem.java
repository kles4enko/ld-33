package org.bitbucket.kles4enko.ld33.core.logic;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.kles4enko.ld33.core.entities.IsoEntity;
import org.bitbucket.kles4enko.ld33.core.level.Level;
import playn.core.Clock;
import pythagoras.f.Vector3;

import static java.lang.Math.abs;
import static java.lang.Math.max;
import static org.bitbucket.kles4enko.ld33.core.util.IsoUtil.*;

/**
 * @author Andrei Kleshchanka
 */
public class CollisionSystem {

    private static final Logger LOG = LogManager.getLogger(CollisionSystem.class);

    public void handleCollisions(Clock clock, Level level) {
        level.getMap().stream().filter(IsoEntity::canMove).forEach(entity -> collideWithLevel(clock, level, entity));
    }

    public boolean collideWithLevel(final Clock clock, final Level level, IsoEntity entity) {
        final Vector3 normPos = normalizedTilePosition(entity.getIsoPosition());
        final Vector3 neighN = getNeighN(normPos);
        final Vector3 neighE = getNeighE(normPos);
        final Vector3 neighS = getNeighS(normPos);
        final Vector3 neighW = getNeighW(normPos);
        final Vector3 neighNE = getNeighNE(normPos);
        final Vector3 neighSE = getNeighSE(normPos);
        final Vector3 neighSW = getNeighSW(normPos);
        final Vector3 neighNW = getNeighNW(normPos);

        final int[] colCountHolder = new int[1];
        colCountHolder[0] = 0;
        level.getMap().stream()
                .filter(isoEntity -> !isoEntity.isEmptyFloor())
                .filter(notFloor -> !notFloor.canMove())
                .filter(e -> !e.isPlayer())
                .filter(wall -> wall.getIsoPosition().equals(neighN)
                        || wall.getIsoPosition().equals(neighE)
                        || wall.getIsoPosition().equals(neighS)
                        || wall.getIsoPosition().equals(neighW)
                        || wall.getIsoPosition().equals(neighNE)
                        || wall.getIsoPosition().equals(neighSE)
                        || wall.getIsoPosition().equals(neighSW)
                        || wall.getIsoPosition().equals(neighNW))
                .filter(possibleCollision -> entity.getRectangle().intersects(possibleCollision.getRectangle()))
                .peek(collision -> colCountHolder[0]++)
                .forEach(neigh -> collideWithTile(entity, neigh));
        return 0 != colCountHolder[0];
    }

    protected void collideWithTile(IsoEntity entity, IsoEntity neigh) {
        Vector3 isoPosition = entity.getIsoPosition();
        Vector3 distVector = getTileCenter(isoPosition).subtractLocal(getTileCenter(neigh.getIsoPosition()));
        float xDepth = 1F - abs(distVector.x);
        float yDepth = 1F - abs(distVector.y);
        if (xDepth > 0 && yDepth > 0) {
            if (max(xDepth, 0F) < max(yDepth, 0F)) {
                if (distVector.x < 0) {
                    isoPosition.addLocal(-xDepth, 0, 0);
                } else {
                    isoPosition.addLocal(xDepth, 0, 0);
                }
            } else {
                if (distVector.y < 0) {
                    isoPosition.addLocal(0, -yDepth, 0);
                } else {
                    isoPosition.addLocal(0, yDepth, 0);
                }
            }
        }
    }
}
