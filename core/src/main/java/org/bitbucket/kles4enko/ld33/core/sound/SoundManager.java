package org.bitbucket.kles4enko.ld33.core.sound;

import playn.core.Platform;
import playn.core.Sound;

/**
 * @author Andrei Kleshchanka
 */
public class SoundManager {

    private final Platform plat;
    private final Sound    gameSound;
    private final Sound    introSound;

    public SoundManager(Platform plat) {
        this.plat = plat;
        this.gameSound = plat.assets().getSound("sounds/GameSound");
        this.introSound = plat.assets().getSound("sounds/IntroSound");
        gameSound.prepare();
        introSound.prepare();
        introSound.play();

    }

    public Sound getGameSound() {
        return gameSound;
    }


    public Sound getIntroSound() {
        return introSound;
    }
}
