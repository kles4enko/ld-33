package org.bitbucket.kles4enko.ld33.core.util;

import pythagoras.f.Vector3;

import static org.bitbucket.kles4enko.ld33.core.util.Constants.*;

/**
 * @author Andrei Kleshchanka
 */
public final class IsoUtil {

    private static final float HALF_TILE = 0.5F;
    private static final float FULL_TILE = 1F;

    /**
     * from iso to screen
     *
     * @param isoPosition - <code>Vector3</code> isometric position
     * @return <code>Vector3</code> screen position
     */
    public static Vector3 projectPosition(Vector3 isoPosition) {
        float x = (isoPosition.x - isoPosition.y) * TILE_WIDTH_HALF;
        float y = (isoPosition.x + isoPosition.y - isoPosition.z) * TILE_HEIGHT_HALF;
        return new Vector3(x, y, isoPosition.z);
    }

    /**
     * from screen to iso
     *
     * @param position - <code>Vector3</code> screen position
     * @return <code>Vector3</code> isometric position
     */
    public static Vector3 unProjectPosition(Vector3 position) {
        float isoX = (float) Math.floor(position.x / TILE_WIDTH + position.y / TILE_HEIGHT + position.z / 2);
        float isoY = (float) Math.floor(position.y / TILE_HEIGHT - position.x / TILE_WIDTH + position.z / 2);
        return new Vector3(isoX, isoY, position.z);
    }

    public static float calculateDepth(Vector3 isoPosition) {
        return (isoPosition.x + isoPosition.y + isoPosition.z) * 0.866F;
    }

    /**
     * Returns normalized grid-based isometric tile position
     *
     * @param isoPosition - tile position
     * @return - normalized tile position
     */
    public static Vector3 normalizedTilePosition(Vector3 isoPosition) {
        return new Vector3(Math.round(isoPosition.x), Math.round(isoPosition.y), 0F);
    }

    public static Vector3 gerIsoPositionDraw(Vector3 isoPosition) {
        return new Vector3(isoPosition.x + HALF_TILE, isoPosition.y + HALF_TILE, isoPosition.z);
    }

    /**
     * Get tile bottom center position
     *
     * @param isoPosition - <code>Vector3</code> screen tile position
     * @return <code>Vector3</code> center position of tile
     */
    public static Vector3 getTileCenter(Vector3 isoPosition) {
        return new Vector3(isoPosition.x - HALF_TILE, isoPosition.y + HALF_TILE, isoPosition.z);
    }

    public static Vector3 getNeighN(Vector3 myIsoCenter) {
        return new Vector3(myIsoCenter.x, myIsoCenter.y - FULL_TILE, myIsoCenter.z);
    }

    public static Vector3 getNeighE(Vector3 myIsoCenter) {
        return new Vector3(myIsoCenter.x + FULL_TILE, myIsoCenter.y, myIsoCenter.z);
    }

    public static Vector3 getNeighS(Vector3 myIsoCenter) {
        return new Vector3(myIsoCenter.x, myIsoCenter.y + FULL_TILE, myIsoCenter.z);
    }

    public static Vector3 getNeighW(Vector3 myIsoCenter) {
        return new Vector3(myIsoCenter.x - FULL_TILE, myIsoCenter.y, myIsoCenter.z);
    }

    public static Vector3 getNeighNE(Vector3 myIsoCenter) {
        return new Vector3(myIsoCenter.x + FULL_TILE, myIsoCenter.y - FULL_TILE, myIsoCenter.z);
    }

    public static Vector3 getNeighSE(Vector3 myIsoCenter) {
        return new Vector3(myIsoCenter.x + FULL_TILE, myIsoCenter.y + FULL_TILE, myIsoCenter.z);
    }

    public static Vector3 getNeighSW(Vector3 myIsoCenter) {
        return new Vector3(myIsoCenter.x - FULL_TILE, myIsoCenter.y + FULL_TILE, myIsoCenter.z);
    }

    public static Vector3 getNeighNW(Vector3 myIsoCenter) {
        return new Vector3(myIsoCenter.x - FULL_TILE, myIsoCenter.y - FULL_TILE, myIsoCenter.z);
    }
}
