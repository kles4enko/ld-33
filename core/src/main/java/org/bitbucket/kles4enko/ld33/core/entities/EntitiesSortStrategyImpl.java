package org.bitbucket.kles4enko.ld33.core.entities;

import org.bitbucket.kles4enko.ld33.core.util.IsoUtil;
import pythagoras.f.Vector3;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Andrei Kleshchanka
 */
public class EntitiesSortStrategyImpl implements EntitiesSortStrategy {

    private static final Comparator<IsoEntity> DEPTH_COMPARATOR = (e1, e2) -> Float.compare(e1.getIsoDepth(), e2.getIsoDepth());

    @Override
    public List<IsoEntity> sortEntities(final List<IsoEntity> entities) {
        return entities.stream().peek(entity -> {
            Vector3 isoPos = entity.getIsoPosition();
            entity.setIsoDepth(IsoUtil.calculateDepth(isoPos));
        }).sorted(DEPTH_COMPARATOR).collect(Collectors.toList());
    }
}
