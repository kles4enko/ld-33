package org.bitbucket.kles4enko.ld33.core.entities;

import java.util.List;

/**
 * @author Andrei Kleshchanka
 */
public interface EntitiesSortStrategy {

    List<IsoEntity> sortEntities(List<IsoEntity> entities);
}
