package org.bitbucket.kles4enko.ld33.core.logic;

import com.google.common.base.MoreObjects;
import com.google.common.base.Stopwatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.kles4enko.ld33.core.control.MonsterController;
import org.bitbucket.kles4enko.ld33.core.entities.EntitiesSortStrategy;
import org.bitbucket.kles4enko.ld33.core.entities.Player;
import org.bitbucket.kles4enko.ld33.core.graphics.Camera;
import org.bitbucket.kles4enko.ld33.core.level.Level;
import playn.core.Clock;

/**
 * @author Andrei Kleshchanka
 */
public class Updater {

    private static final Logger LOG = LogManager.getLogger(Updater.class);
    private final MonsterController    controller;
    private final EntitiesSortStrategy sortStrategy;
    private final CollisionSystem      collisionSystem;
    private final Camera               camera;
    private final Player               player;
    private final Stopwatch            stopwatch;

    public Updater(MonsterController controller, EntitiesSortStrategy sortStrategy, CollisionSystem collisionSystem, Camera camera, Player player, Stopwatch stopwatch) {
        this.controller = controller;
        this.sortStrategy = sortStrategy;
        this.collisionSystem = collisionSystem;
        this.camera = camera;
        this.player = player;
        this.stopwatch = stopwatch;
        LOG.debug("Initialized : {}", this);
    }

    public Level update(final Clock clock, final Level level) {
        LOG.trace("update");
        // update input
        controller.update(clock);
        // collisions
        collisionSystem.handleCollisions(clock, level);
        // update camera position
        camera.update(player.getIsoPosition());
        // prepare for render
        level.setMap(sortStrategy.sortEntities(level.getMap()));
        return level;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("sortStrategy", sortStrategy)
                .toString();
    }
}
