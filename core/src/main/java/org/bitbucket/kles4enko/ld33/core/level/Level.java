package org.bitbucket.kles4enko.ld33.core.level;

import org.bitbucket.kles4enko.ld33.core.entities.IsoEntity;
import org.bitbucket.kles4enko.ld33.core.entities.Player;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * @author Andrei Kleshchanka
 */
public class Level {

    private List<IsoEntity> map;
    private Player          player;

    public Level(@Nonnull List<IsoEntity> map, @Nonnull Player player) {
        this.map = map;
        this.player = player;
    }


    public List<IsoEntity> getMap() {
        return map;
    }

    public void setMap(List<IsoEntity> map) {
        this.map = map;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }
}
