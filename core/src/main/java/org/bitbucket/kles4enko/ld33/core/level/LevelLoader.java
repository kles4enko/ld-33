package org.bitbucket.kles4enko.ld33.core.level;

import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.kles4enko.ld33.core.entities.Enemy;
import org.bitbucket.kles4enko.ld33.core.entities.IsoEntity;
import org.bitbucket.kles4enko.ld33.core.entities.Player;
import org.bitbucket.kles4enko.ld33.core.graphics.TileManager;
import playn.core.Image;
import playn.core.Platform;
import pythagoras.f.Vector3;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Andrei Kleshchanka
 */
public class LevelLoader {

    public static final char EMPTY_FLOOR_TILE_TYPE = ' ';

    private static final Logger LOG = LogManager.getLogger(LevelLoader.class);
    private final Platform        plat;
    private final String          pathToLevelFile;
    private final TileManager     tileManager;
    private       List<IsoEntity> map;
    private       Player          player;

    public LevelLoader(Platform plat, String pathToLevelFile, TileManager tileManager) {
        this.plat = plat;
        this.pathToLevelFile = pathToLevelFile;
        this.tileManager = tileManager;
        this.map = new ArrayList<>();
    }

    public Level loadLevel() {
        String levelAsString = null;
        try {
            levelAsString = plat.assets().getTextSync(pathToLevelFile);
        } catch (Exception e) {
            LOG.error("Couldn't load file : {}", pathToLevelFile);
        }
        char[][] levelData = convertTo2DArray(levelAsString);
        LOG.debug("Level data : {}", Arrays.toString(levelData));

        final float SHIFT_X = 10;
        final float SHIFT_Y = 0;

        for (int y = 0; y < levelData.length; y++) {
            for (int x = 0; x < levelData[y].length; x++) {
                Character tileType = levelData[y][x];
                float xx = x + SHIFT_X;
                float yy = y + SHIFT_Y;
                placeLevelTile(new Vector3(xx, yy, 0), tileType);
            }
        }

        return new Level(map, player);
    }

    private void placeLevelTile(final Vector3 point, char tileType) {
        Image image = tileManager.getTile(tileType);
        IsoEntity entity;
        switch (tileType) {
            case 'P':
                entity = initPlayer(point, image, tileType);
                break;
            case 'D':
                entity = initEnemy(point, image, tileType);
                break;
            default:
                entity = new IsoEntity(new Vector3(point), 0F, image.state, tileType);
                break;
        }
        LOG.debug("New entity has been created : {}", entity);
        map.add(entity);
    }

    private IsoEntity initEnemy(Vector3 point, Image image, char tileType) {
        Enemy enemy = new Enemy(new Vector3(point.x, point.y, 1F), 0F, image.state, tileType);
        map.add(enemy);
        Image floorImage = tileManager.getTile(EMPTY_FLOOR_TILE_TYPE);
        return new IsoEntity(new Vector3(point), 0F, floorImage.state, EMPTY_FLOOR_TILE_TYPE);
    }

    private IsoEntity initPlayer(final Vector3 point, Image image, char tileType) {
        player = new Player(new Vector3(point.x, point.y, 1F), 0F, image.state, tileType);
        map.add(player);
        Image floorImage = tileManager.getTile(EMPTY_FLOOR_TILE_TYPE);
        return new IsoEntity(new Vector3(point), 0F, floorImage.state, EMPTY_FLOOR_TILE_TYPE);
    }

    @Nonnull
    private char[][] convertTo2DArray(@Nullable final String resourceString) {
        if (StringUtils.isEmpty(resourceString)) {
            return new char[0][0];
        }
        String[] split = StringUtils.split(resourceString, StringUtils.CR + StringUtils.LF);
        char[][] result = new char[split.length][];

        for (int i = 0; i < split.length; i++) {
            String s = split[i];
            result[i] = new char[s.length()];
            s.getChars(0, s.length(), result[i], 0);
        }
        return result;
    }
}
