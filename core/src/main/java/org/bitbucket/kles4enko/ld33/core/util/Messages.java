package org.bitbucket.kles4enko.ld33.core.util;

import java.text.MessageFormat;
import java.util.ResourceBundle;

/**
 * @author Andrei Kleshchanka
 */
public class Messages {

    private static final ResourceBundle RESOURCE = ResourceBundle.getBundle(Constants.MESSAGES_BUNDLE_PATH);

    public static String getText(String key, Object... args) {
        return MessageFormat.format(RESOURCE.getString(key), args);
    }
}
