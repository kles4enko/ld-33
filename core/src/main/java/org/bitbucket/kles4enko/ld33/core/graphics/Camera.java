package org.bitbucket.kles4enko.ld33.core.graphics;

import org.bitbucket.kles4enko.ld33.core.util.IsoUtil;
import pythagoras.f.IDimension;
import pythagoras.f.MathUtil;
import pythagoras.f.Vector3;

/**
 * @author Andrei Kleshchanka
 */
public class Camera {

    private final IDimension viewSize;
    private final Vector3 position = new Vector3();

    public Camera(IDimension viewSize) {
        this.viewSize = viewSize;
    }

    public void update(Vector3 playerPositionIso) {
        Vector3 projectPosition = IsoUtil.projectPosition(playerPositionIso);
        position.x = MathUtil.lerp(position.x, -projectPosition.x + viewSize.width() / 2, 0.1F);
        position.y = MathUtil.lerp(position.y, -projectPosition.y + viewSize.height() / 2, 0.1F);
    }

    public Vector3 getPosition() {
        return position;
    }
}
