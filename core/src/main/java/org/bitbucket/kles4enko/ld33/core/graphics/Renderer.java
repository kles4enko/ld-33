package org.bitbucket.kles4enko.ld33.core.graphics;

import com.google.common.base.MoreObjects;
import com.google.common.base.Stopwatch;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bitbucket.kles4enko.ld33.core.entities.IsoEntity;
import org.bitbucket.kles4enko.ld33.core.entities.Player;
import playn.core.*;
import pythagoras.f.IDimension;
import pythagoras.f.Vector3;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.bitbucket.kles4enko.ld33.core.util.Constants.TILE_HEIGHT;
import static org.bitbucket.kles4enko.ld33.core.util.IsoUtil.gerIsoPositionDraw;
import static org.bitbucket.kles4enko.ld33.core.util.IsoUtil.projectPosition;

/**
 * @author Andrei Kleshchanka
 */
public class Renderer {

    private static final Logger LOG = LogManager.getLogger(Renderer.class);
    private final Surface    surface;
    private final Platform   plat;
    private final UIManager  uiManager;
    private final Camera     camera;
    private final Graphics   graphics;
    private final IDimension viewSize;
    private final Stopwatch  stopwatch;
    private final Player     player;
    public float r, g, b, a = 1F;

    public Renderer(Surface surface, Platform plat, Camera camera, Stopwatch stopwatch, Player player) {
        this.plat = plat;
        this.stopwatch = stopwatch;
        this.player = player;
        this.graphics = plat.graphics();
        this.surface = surface;
        this.camera = camera;
        this.uiManager = new UIManager(plat);
        this.viewSize = plat.graphics().viewSize;
        setBackground();
        LOG.debug("Initialized : {}", this);
    }

    public void render(Clock renderClock, List<IsoEntity> map) {
        LOG.trace("render");
        surface.clear(r, g, b, a);
        surface.saveTx();
        surface.begin();
        drawCells(surface);
        surface.startClipped(0, 0, (int) viewSize.width(), (int) viewSize.height());
        boolean noDraw = drawBlackScreenIfLoose(surface);
        try {
            if (!noDraw) {
                surface.saveTx();
                try {
                    surface.translate(camera.getPosition().x, camera.getPosition().y);
                    drawMap(map);
                } finally {
                    surface.restoreTx();
                }
                uiManager.renderWithBlackBg(surface, String.format("Time: %.2f", 30F - stopwatch.elapsed(TimeUnit.MILLISECONDS) / 1000F), viewSize.width() / 2, 30, 96, 24);
            }
        } finally {
            surface.endClipped();
            surface.end();
            surface.restoreTx();
        }
    }

    private boolean drawBlackScreenIfLoose(Surface surface) {
        if ((30F - stopwatch.elapsed(TimeUnit.MILLISECONDS) / 1000F) < 0F) {
            uiManager.render(surface, "Game Over. Your result: " + player.getScore(), viewSize.width() / 2 - 20, viewSize.height() / 2);
            return true;
        }
        return false;
    }

    private void drawCells(Surface surface) {
        surface.setFillColor(Color.rgb(50, 50, 50));
        int step = (int) TILE_HEIGHT;
        float width = viewSize.width();
        float height = graphics.viewSize.height();
        for (int x = 0; x < width / step; x++) {
            for (int y = 0; y < height / step; y++) {
                surface.drawLine(x * step, y * step, width, y * step, 1);
                surface.drawLine(x * step, y * step, x * step, height, 1);
            }
        }
    }

    private void drawMap(List<IsoEntity> map) {
        for (IsoEntity entity : map) {
            Vector3 isoPosition = entity.getIsoPosition();
            Vector3 drawIsoPosition = gerIsoPositionDraw(isoPosition);
            Vector3 screenPosition = projectPosition(drawIsoPosition);
            Tile tile = entity.getTile();

            surface.draw(tile, screenPosition.x, screenPosition.y - tile.height());

            if (entity.isPlayer()) {
                uiManager.render(surface, "Kill all doctors and kids!", 10, 35);
                uiManager.render(surface, "Find and kill you wife!", 10, 50);
                uiManager.render(surface, "You have 30 SECONDS ONLY!!!", 10, 65);
                uiManager.render(surface, "Controls: UP,DOWN,LEFT,RIGHT and Z for attack", 240, 40);
            }
        }
    }

    public void setBackground(float r, float g, float b, float a) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a;
    }

    private void setBackground() {
        // set default background
        r = 32 / 255F;
        g = 32 / 255F;
        b = 32 / 255F;
    }


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("a", a)
                .add("r", r)
                .add("g", g)
                .add("b", b)
                .add("surface", surface)
                .add("plat", plat)
                .add("uiManager", uiManager)
                .toString();
    }
}
