package org.bitbucket.kles4enko.ld33.core.graphics;

import com.google.common.base.MoreObjects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import playn.core.Image;
import playn.core.Platform;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static org.bitbucket.kles4enko.ld33.core.util.PlayNUtil.PIXELART_TEXTURE_CONFIG;

/**
 * @author Andrei Kleshchanka
 */
public class TileManager {

    private static final Logger LOG = LogManager.getLogger(TileManager.class);

    private final Map<Character, Image> tilesData;
    private final Platform              plat;
    // textures
    private final Image                 floorSimpleTile;
    private final Image                 wallSimpleTile;
    private final Image                 wallHighTile;
    private final Image                 monsterDummy;
    private final Image                 player;
    private final Image                 doctorAlive;
    private final Image                 doctorDied;


    public TileManager(Platform plat) {
        this.plat = plat;
        this.tilesData = new HashMap<>();
        // textures
        this.floorSimpleTile = getImage("floorSimple");
        this.wallSimpleTile = getImage("wallSimple");
        this.wallHighTile = getImage("wallHigh");
        this.monsterDummy = getImage("monsterDummy");
        this.player = getImage("Player");
        this.doctorAlive = getImage("doctorAlive");
        this.doctorDied = getImage("doctorDied");

        // link
        linkTiles();
        LOG.info("initialized : {}", this);
    }

    private void linkTiles() {
        tilesData.put('W', wallHighTile);
        tilesData.put('w', wallSimpleTile);
        tilesData.put(' ', floorSimpleTile);
        tilesData.put('P', player);
        tilesData.put('D', doctorAlive);
        tilesData.put('d', doctorDied);

    }

    private Image getImage(String fileName) {
        return plat.assets().getImage(String.format("tiles/%s.png", fileName)).setConfig(PIXELART_TEXTURE_CONFIG);
    }

    public Image getTile(Character tileType) {
        Image image = tilesData.get(tileType);
        if (Objects.isNull(image)) {
            throw new RuntimeException("Can't find image for tile type : " + tileType);
        }
        return image;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("floorSimpleTile", floorSimpleTile)
                .add("tilesData", tilesData)
                .add("plat", plat)
                .add("wallSimpleTile", wallSimpleTile)
                .add("wallHighTile", wallHighTile)
                .toString();
    }
}
